// Global Event for Rank System
// Cart Button Event
$(".showCart").on("click", function() {
    showCart();
    console.log('button showCart')
});
// Modal Button Event
// To close modal if user click outside the modal
$(".closeModal").on("click", function() {
    if ($('.modal').css("display", "flex")) {
        $('.modal').css("display", "none");
    }
    console.log('pressing cart')
})

function showCart() {
    //  Make sure to empty up the modal first
    $("#cartResult").empty()
    $(".totalPrice").empty()
    $(".modal").css('display', 'flex');
    $.ajax({
        url: $("meta[name='cartListCartMeta']").attr("content"),
        type: "GET",
        success: function(res) {
            // console.log(res)
            console.log(res.data)
            var pyData = res.data
            var total_price = 0;
            for (i in pyData) {
                total_price += parseInt(pyData[i].price) * parseInt(pyData[i].quantity)
                var temp = '<div class="product1">'
                temp += '<div class="cartResultDetail"><h4>' + pyData[i].product_name + '</h4><br><p>' + new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDN' }).format(pyData[i].price) +
                    "</p></div><div class='cartResultQty'><button id='" + pyData[i].entry_id + "'class='deleteItemQty'>Delete</button><h5>" + pyData[i].quantity +
                    "</h5><button id='" + pyData[i].entry_id + "' class='addItemQty'>Add</button></div>"
                $("#cartResult").append(temp);
            }
            $(".modalContainer").append("</div>");
            $(".totalPrice").append("Total : " + new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDN' }).format(total_price));

        }
    })

}

// cartResultQty JS
$("#cartResult").click((e) => {
    if ($(e.target).hasClass("deleteItemQty")) {
        $.ajax({
            url: $("meta[name='deleteItemQtyCartMeta']").attr("content"),
            type: "POST",
            data: {
                id_target: $(e.target).attr("id")
            },
            success: function() {
                showCart()
                alert("Item deleted")
            },
        })
    } else if ($(e.target).hasClass("addItemQty")) {
        $.ajax({
            url: $("meta[name='addItemQtyCartMeta']").attr("content"),
            type: "POST",
            data: {
                id_target: $(e.target).attr("id")
            },
            success: function() {
                showCart()
                alert("Item added")
            }
        })
    }
})

// Button AddToCart
$("#addToCart").click(function() {
    var holder = $(".product_name").attr("id")
        // alert(holder)
    $.post($("meta[name='createCartMeta']").attr("content"), {
        product_name: holder,
    }).done(() => {
        alert("This product is successfully added to your cart")
    })
})