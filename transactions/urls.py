from django.urls import path
from . import views

app_name = "transactions"

urlpatterns = [
    path("history/", views.history_view, name="history"),
    path("checkout/", views.checkout, name="checkout"),
    path("add_transaction/", views.add_transaction, name="add")
]