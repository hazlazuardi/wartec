from django.shortcuts import render, redirect
from .models import Transaction
from dashboard.models import Product
from coupon.models import Coupon
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from checkout.models import Checkout

def get_total_price(trans):
    total_price = 0
    for item_transaction in trans.item.all():
        total_price += item_transaction.product.price * item_transaction.quantity
    
    if trans.coupon != None:
        return int(total_price*(100-trans.coupon.percentage)/100)
    else:
        return total_price

@login_required(login_url='/accountLogin/')
def history_view(request):
    transaction_list = [
        (trans, get_total_price(trans), ", ".join(map(str,trans.item.all()))) for trans in Checkout.objects.filter(user = request.user).order_by("-date").all()
    ]
    context = {
        "transaction_list" : transaction_list
    }
    return render(request, "transactions/history.html", context)

@login_required(login_url='/accountLogin/')
def checkout(request):
    if request.method == "POST":
        # Checkout item details
        name = request.POST["name"]
        price = request.POST["price"]
        description = request.POST["description"]
        
        # To search for coupons
        try:
            coupon_name = request.POST["nama_kupon"] if request.POST["nama_kupon"] else ""
            coupon_percentage = request.POST["persentase_kupon"] if request.POST["persentase_kupon"] else 0
            coupon_list = Coupon.objects.filter(name__icontains = coupon_name)
            coupon_list = coupon_list.filter(percentage__gte = coupon_percentage)
            coupon_list = coupon_list.filter(expiry__gte = timezone.now(), min_price__lte = price) #Checking the expiry date and min price
        except:
            coupon_list = Coupon.objects.filter(expiry__gte = timezone.now(), min_price__lte = price)[:10]
            coupon_name = ""
            coupon_percentage = ""

        purchased_item = Product.objects.get(
            name = name,
            price = price,
            description = description
        )

        # To choose a coupon
        try:
            chosen_coupon = request.POST["kupon_terpilih"].split("---")
            chosen_coupon_name = chosen_coupon[0]
            chosen_coupon_percentage = chosen_coupon[1]
            price_after_discount = int(purchased_item.harga_barang * (100-float(chosen_coupon_percentage)) / 100)
        except:
            chosen_coupon_name = ""
            chosen_coupon_percentage = ""
            price_after_discount = None

        context = {
            "purchased_item" : purchased_item,
            "coupon_list" : coupon_list,
            "coupon_name" : coupon_name,
            "coupon_percentage" : coupon_percentage,
            "chosen_coupon_name" : chosen_coupon_name,
            "chosen_coupon_percentage" : chosen_coupon_percentage,
            "price_after_discount" : price_after_discount
        }

        return render(request, "transactions/checkout.html", context)
    else:
        return redirect("dashboard:landing")

def add_transaction(request):
    if request.method == "POST":
        # Details for items in checkout
        name = request.POST["name"]
        price = request.POST["price"]
        description = request.POST["description"]

        # Buyer details
        buyer_name = request.POST["first_name"] + " " + request.POST["last_name"]

        # Details for purchased item
        purchased_item = Product.objects.get(
            name = name,
            price = price,
            description = description
        )

        # Submit choosen coupon to the hidden form
        try:
            chosen_coupon_name = request.POST["kupon_terpilih_nama"]
            chosen_coupon_percentage = request.POST["kupon_terpilih_persentase"]
            chosen_coupon = Coupon.objects.get(name=chosen_coupon_name, percentage = chosen_coupon_percentage)
        except:
            chosen_coupon = None

        # Create new transaction
        new_transaction = Transaction.objects.create(
            name = buyer_name,
            price = purchased_item,
            kupon = chosen_coupon
        ).save()
        
        purchased_item.save()

    return redirect("dashboard:landing")