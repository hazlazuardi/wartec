from django.db import models
from dashboard.models import Product
from coupon.models import Coupon

class Transaction(models.Model):
    @property
    def get_transaction(self):
        try:
            return self.barang.harga_barang * (100 - self.kupon.percentage) // 100
        except:
            return self.barang.harga_barang #Coupons are optional

    buyer_name = models.TextField(max_length=50)
    item = models.ForeignKey(Product, on_delete=models.PROTECT)
    total_transaction = get_transaction
    coupon = models.ForeignKey(Coupon, on_delete=models.PROTECT,null=True, blank=True) #Coupons are optional
    buy_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {} @ {}".format(self.buyer_name, self.item, self.buy_date)
    objects = models.Manager()
    
        

    