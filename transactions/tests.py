from django.test import TestCase, Client
from django.apps import apps
from django.urls import reverse, resolve

from .apps import TransactionsConfig

# Create your tests here.
class TransactionsTest(TestCase):
    def test_app(self):
        self.assertEqual(TransactionsConfig.name, 'transactions')
        self.assertEqual(apps.get_app_config('transactions').name, 'transactions')
    
    def setUp(self):
        self.client = Client()
        self.index_url = reverse("transactions:history")
        
    def test_landing_page_redirects_without_login(self):
        response = self.client.get(self.index_url)
        self.assertEquals(302, response.status_code)
    