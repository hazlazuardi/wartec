from django.contrib import admin
from .models import Cart, Entry
# Register your models here.


class EntryAdmin(admin.ModelAdmin):
    list_display = ['product','quantity']
    list_editable = ['quantity']

admin.site.register(Entry, EntryAdmin)


class CartAdmin(admin.ModelAdmin):
    # For the column of the table in admin' dashboard
    list_display = ('owner', 'items_count',)
    def items_count(self, obj):
        return obj.items.all().count()    
            
admin.site.register(Cart, CartAdmin)
