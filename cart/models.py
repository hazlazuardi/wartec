from django.db import models

from dashboard.models import Product
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

# Create your models here.
class Entry(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()

    class Meta:
        ordering = ('product',)
        verbose_name = 'entry'
        verbose_name_plural = 'entries'

    def __str__(self):
        return f"{self.product.name} ({self.quantity})"
    objects = models.Manager()


class Cart(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    items = models.ManyToManyField(Entry)

    class Meta:
        ordering = ('owner',)
        verbose_name = 'cart'
        verbose_name_plural = 'carts'

    def __str__(self):
        return f"{self.owner.username}"
    objects = models.Manager()


