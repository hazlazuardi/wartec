from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

from .models import Cart, Entry
from dashboard.models import Product

# Create your views here.


# Function 1: Display all the items inside the cart
# Return JsonResponse to be retrieved by AJAX
@csrf_exempt
def cartList(request):
    user_cart,created = Cart.objects.get_or_create(owner=request.user)
    user_items = [ {"product_name" : item.product.name, "price" : item.product.price, "quantity" : item.quantity, "entry_id" : item.id} for item in user_cart.items.all()]
    return JsonResponse({'data':user_items})




# Function 2: Create a cart object with User model as the owner
# Return JsonResponse to be retrieved by AJAX
@csrf_exempt
def createCart(request):
    if request.method=="POST":
        cart_item = Product.objects.get(name=request.POST["product_name"])
        result, created = Cart.objects.get_or_create(owner=request.user)
        total_items = [item.product.name for item in result.items.all()]
        if cart_item.name in total_items:
            target = Entry.objects.get(product=cart_item)
            target.quantity += 1
            target.save()
            return JsonResponse({"message" : "failed"})
            
        else :
            item = Entry(product=cart_item,quantity=1)
            item.save()
            cart = Cart.objects.get(owner=request.user)
            cart.items.add(item)
            cart.save()
            return JsonResponse({"message" : "succesfully added"})


# Function 3: Add item to the cart
# Return JsonResponse to be retrieved by AJAX
@csrf_exempt
def addItemQty(request):
    if request.method=="POST":
        id_obj = request.POST['id_target']
        product_obj= get_object_or_404(Entry, id=id_obj)
        itemToAct = Cart.objects.get(owner=request.user)
        if product_obj.product.stock == product_obj.quantity:
            return JsonResponse({'.':'.'})
        else:
            product_obj.quantity += 1
            product_obj.save()
            return JsonResponse({'.':"."})


# Function 4: Delete item inside the cart
# Return JsonResponse to be retrieved by AJAX
@csrf_exempt
def deleteItemQty(request):
    if request.method=="POST":
        id_obj = request.POST['id_target']
        product_obj= get_object_or_404(Entry, id=id_obj)
        itemtoAct = Cart.objects.get(owner=request.user)
        if product_obj.quantity == 1:
            itemtoAct.items.remove(product_obj)
        else :
            product_obj.quantity -= 1
            product_obj.save()
        return JsonResponse({'.':'.'})

