from django.test import TestCase
from django.apps import apps
from .apps import CartConfig
from django.test import Client
from django.urls import resolve
from django.shortcuts import reverse
from .views import cartList, createCart, addItemQty, deleteItemQty

from .models import Cart, Entry
from dashboard.models import Product, Category
from django.contrib.auth.models import User


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import time
import random


# Create your tests here.
class AppsConf(TestCase):
    def test_app(self):
        self.assertEqual(CartConfig.name, 'cart')
        self.assertEqual(apps.get_app_config('cart').name, 'cart')

    

# url= 'http://127.0.0.1:8000/'
url= 'http://wartec.herokuapp.com/'

class GA2FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(GA2FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(GA2FunctionalTest, self).tearDown()

    def test_product_exist_after_added_and_qty_incremented(self):
        selenium = self.selenium
        selenium.get(url+'djangoaccounts/signup/')
        time.sleep(3)

        # Find the form
        username = selenium.find_element_by_id('id_username')
        email = selenium.find_element_by_id('id_username')
        password1 = selenium.find_element_by_id('id_password1')
        password2 = selenium.find_element_by_id('id_password2')

        # Username is unique, handle the already created accounts by using random number
        post = str(random.randint(1000, 10000))

        # Fill the form, not valid
        username.send_keys("hazlazuardi"+post) 
        email.send_keys("hahaha2323"+post+"@gmail.com")
        password1.send_keys('w3w1duararua')
        password2.send_keys('w3w1duararua')
        # Submit, create account
        createaccountbtn = selenium.find_element_by_css_selector('.createaccount')
        createaccountbtn.click()

        # Should be redirected to sign-up path for login
        # already auto-completed, hence, should only need to click loginbtn
        time.sleep(3)
        loginbtn = selenium.find_element_by_css_selector('.loginbtn')
        loginbtn.click()

        # Go to detail
        selenium.implicitly_wait(2)
        prod = selenium.find_element_by_css_selector('.aToDet')
        prod.click()

        # Inside detail
        selenium.implicitly_wait(4)
        desc = selenium.find_element_by_css_selector('.descForTest')
        descIs = desc.get_attribute('innerHTML') == "Description: "
        self.assertTrue(descIs)

        addToCartt = selenium.find_element_by_id('addToCart')
        addToCartt.click()
# 
        try:
            alert_obj = self.driver.switch_to_alert()
        except: 
            return False        
        alert_obj.accept()

        selenium.implicitly_wait(4)
        showCartt = selenium.find_element_by_css_selector('.showCart')
        showCartt.click()

        selenium.implicitly_wait(4)
        qtycheck = selenium.find_element_by_css_selector('.cartResultQty>h5')
        qtyCC = qtycheck.get_attribute('innerHTML') == "1"
        self.assertTrue(qtyCC)

# Add it
        addQty = selenium.find_element_by_css_selector('.addItemQty')
        addQty.click()
        try:
            alert_obj = self.driver.switch_to_alert()
        except: 
            return False        
        alert_obj.accept()
        qtyCC = qtycheck.get_attribute('innerHTML') == "2"
        self.assertTrue(qtyCC)








       

