from django.contrib import admin
from django.urls import path, include

from .views import cartList, createCart, addItemQty, deleteItemQty

app_name = 'cart'

urlpatterns = [
    path('cartList', cartList, name='cartList'),
    path('createCart', createCart, name='createCart'),
    path('addItemQty', addItemQty, name='addItemQty'),
    path('deleteItemQty', deleteItemQty, name='deleteItemQty')
]
