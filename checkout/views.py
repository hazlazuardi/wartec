from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from cart.models import Cart
from .models import Checkout
from coupon.models import Coupon
from django.contrib.auth.decorators import login_required

@login_required(login_url='/accountLogin/')
def submit(request):
    kerown = Cart.objects.get(owner=request.user)
    ccart = kerown.items.all()
    checkout_list = {
        tb : tb.product.price * tb.quantity for tb in ccart #A list of items and their quantities in the checkout cart 
    }

    total_price = 0
    for price in checkout_list.values():
        total_price += price

    context = {
        "total_harga" : total_price,
        "checkout_list" : checkout_list.items(),
        "checkout_page" : True
    }
    return render(request, "checkout/transaksicepat.html", context)

@csrf_exempt
def process(request):
    kerown = Cart.objects.get(owner=request.user)
    ccart = kerown.items.all()
    if request.method == "POST" and request.is_ajax():
        for item_purchase in ccart:
            purchase_quantity = item_purchase.quantity
        coupon_name = request.POST["nama_kupon_dipilih"]
        chosen_coupon_instance = None
        if coupon_name != "None":
            chosen_coupon_instance = Coupon.objects.get(name = coupon_name)
        
        new_trans = Checkout.objects.create(
            user = request.user,
            coupon = chosen_coupon_instance
        )
        
        # Add item to checkout
        for tb in ccart:
            new_trans.item.add(tb)
            item_instance = tb.product
            item_instance.save()
        new_trans.save()

        # Emptying user's cart.
        kerown.items.clear()
        return JsonResponse({"status" : "success"})