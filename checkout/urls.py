from django.urls import path, include
from . import views

app_name = 'checkout'
urlpatterns = [
    path('process/', views.process, name="process"),
    path("submit/", views.submit, name="submit")
]