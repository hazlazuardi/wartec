from django.test import LiveServerTestCase, TestCase, Client
from django.apps import apps
from django.contrib.auth.models import User
from cart.models import Cart, Entry
from django.shortcuts import reverse
import time
import os
from .models import Checkout
from dashboard.models import Product
from cart.models import Cart
from coupon.models import Coupon

import datetime

from django.contrib.auth.models import User

from .apps import CheckoutConfig

# Create your tests here.
class CheckoutTest(TestCase):
    def test_app(self):
        self.assertEqual(CheckoutConfig.name, 'checkout')
        self.assertEqual(apps.get_app_config('checkout').name, 'checkout')
    
    def test_page_exists(self):
        response = Client().get('/checkout/process')
        self.assertEqual(301, response.status_code)

    def test_landing_page_redirects_without_login(self):
        response = Client().get('/checkout/submit')
        self.assertEquals(301, response.status_code)


    


    
   
