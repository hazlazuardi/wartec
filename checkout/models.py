from django.db import models
from django.contrib.auth.models import User
from cart.models import Entry, Cart
from coupon.models import Coupon

# Create your models here.
class Checkout(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ManyToManyField(Entry)
    coupon = models.ForeignKey(Coupon, on_delete=models.CASCADE, default= None, blank=True, null=True)
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"Transaction from user {self.user} using the coupon {self.coupon} on {self.date}"
    objects = models.Manager()
