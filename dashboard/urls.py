from django.contrib import admin
from django.urls import path, include

from .views import landing, product_detail

app_name = 'dashboard'

urlpatterns = [
    path('', landing, name='landing'),
    path('product/<slug:slug>/<int:index>', product_detail, name='product_detail')
]
