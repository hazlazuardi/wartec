# admin.py
from django.contrib import admin
from .models import Category, Product

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name','slug']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Category,CategoryAdmin)

class ProductAdmin(admin.ModelAdmin):
    # For the column of the table in admin' dashboard
    list_display = ['name','slug', 'category', 'price', 'stock']
    # For filtering
    list_filter = ['category']
    # For quick edit in admin's dashboard
    list_editable = ['price', 'stock']
    # Auto fill for slug field if we fill name field
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Product, ProductAdmin)
