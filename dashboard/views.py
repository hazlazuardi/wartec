from django.shortcuts import render
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required

from .models import Product
# Create your views here.

# Function 1: Landing page, can search any items.
# @login_required(login_url='accountLogin/')
def landing(request):
    context = {}
    url_parameter = request.GET.get("q")
    if url_parameter:
        products = Product.objects.filter(name__icontains=url_parameter)
        category = Product.objects.filter(category__name__icontains=url_parameter)
        context["products"] = products | category
    else:
        products = Product.objects.all()
        context["products"] = products
    if request.is_ajax():
        html = render_to_string(template_name="dashboard/products-results-partial.html", context=context)
        data_dict = {"html_from_view": html}
        return JsonResponse(data=data_dict, safe=False)
    return render(request, 'dashboard/dashboard.html', context)

# Funciton 2: Detail page, to get to a specific item page.
def product_detail(request, index, slug):
    product = Product.objects.get(pk=index)
    return render(request, 'dashboard/product_detail.html', context = {'product':product})