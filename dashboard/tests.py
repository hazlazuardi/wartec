from __future__ import unicode_literals
from django.utils.encoding import force_text

from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Product, Category
from django.contrib.auth.models import User

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time
import random

class TestViews(TestCase):
    def setUp(self):
        self.index_url = reverse("dashboard:landing")
        self.category_test = Category.objects.create(
            name = "category_test"
        )
        self.product_test = Product.objects.create(
            name = "Macbook_Test",
            price = 100000,
            stock = 100,
            description = "This is desc",
            category = self.category_test,
            slug = 'Macbook_Test'
        )
        self.test_barang_detail_url = reverse("dashboard:product_detail",args=[self.product_test.slug, self.product_test.id])

    def test_barang_list_GET(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)

    def test_barang_detail_GET(self):
        response = self.client.get(self.test_barang_detail_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "dashboard/product_detail.html")

    def test_item_search(self):
        self.product_test = Product.objects.create(
            name = "Macbook_Test",
            price = 100000,
            stock = 100,
            description = "This is desc",
            category = self.category_test,
            slug = 'test_barang'
        )
        qe = self.client.get('q')
        response = self.client.get('/', {'q':'Macb'})
        self.assertEquals(response.status_code, 200)

    # def test_success_when_not_added_before(self):
    #     response = self.client.post('/')
    #     self.assertEqual(response.status_code, 200)
    #     self.assertJSONEqual(force_text(response.content), {'status': 'success'})
        
        
        
        # url = 'http://story10haz.herokuapp.com'

