
# Group Assignment 2
This Gitlab repository is the result of the work from **Wishnu Hazmi, Johanes Steven, Naratama, Raka**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/hazlazuardi/wartec/badges/master/pipeline.svg)](https://gitlab.com/hazlazuardi/s10/commits/master)
[![coverage report](https://gitlab.com/hazlazuardi/wartec/badges/master/coverage.svg)](https://gitlab.com/hazlazuardi/s10/commits/master)

## URL
This story can be accessed from [https://wartec.herokuapp.com](https://wartec.herokuapp.com)

