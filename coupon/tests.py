from django.test import TestCase, Client
from django.apps import apps
from django.urls import reverse, resolve
from django.utils import timezone
import datetime

from .apps import CouponConfig
from .models import Coupon

# Create your tests here.

class CouponTest(TestCase):
    def test_app(self):
        self.assertEqual(CouponConfig.name, 'coupon')
        self.assertEqual(apps.get_app_config('coupon').name, 'coupon')

class TestCouponViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse("coupon:coupon-index")
        self.test_kupon = Coupon.objects.create(
            name = "dummy_coupon",
            percentage = 50,
            expiry = datetime.datetime(2050, 5, 17),
            min_price = 10000
        )

    def test_coupon_GET(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "coupon/kupon.html")

    def test_coupon_POST(self):
        response = self.client.post(self.index_url, {
            "name" : "test",
            "percentage" : "",
            "total_harga" : 0
        })
        self.assertEquals(response.status_code, 200)

class TestCouponModels(TestCase):
    def setUp(self):
        self.coupon_test = Coupon.objects.create(
            name = "dummy_coupon",
            percentage = 50,
            expiry = timezone.now(),
            min_price = 10000
        )
    
    def test_coupon_name(self):
        self.assertEquals(str(self.coupon_test), "dummy_coupon")


    