from django.shortcuts import render
from django.http import JsonResponse
from .models import Coupon
from .forms import CouponForm
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt

from datetime import date

@csrf_exempt
def index(request):
    qs = Coupon.objects.all()
    context = {
        'queryset' : qs,
        'form' : CouponForm()
    }

    if request.method == 'POST':
        name = request.POST['name']
        percentage = request.POST['percentage'] if request.POST['percentage'] != '' else 0
        price = request.POST['total_harga']

        # Filter for coupon
        qs = qs.filter(name__icontains=name)
        qs = qs.filter(percentage__gte = percentage )
        qs = qs.filter(min_price__lte = price)
        qs = qs.filter(Q(expiry__gte = date.today()))
        context['queryset'] = qs

        result = {}
        for coupon in qs.all():
            result[str(coupon)] = {
                "percentage" : coupon.percentage,
                "minimum_price" : coupon.min_price
            }

        if request.is_ajax():
            return JsonResponse(result)

        return render(request, 'coupon/kupon.html', context)
    return render(request, 'coupon/kupon.html', context)

    
    