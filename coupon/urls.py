from django.urls import path, include
from . import views

app_name = 'coupon'

urlpatterns = [
    path('', views.index, name="coupon-index"),
]
