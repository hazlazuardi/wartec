$(document).ready(function(e) {
    var total_harga = Number($("#total-harga").text())
    $("#checkout-btn").click(() => {
        var total_harga = $("#new-harga-amount").text() ? Number($("#new-harga-amount").text()) : Number($("#total-harga").text())
        $("#notification-sedang-proses").css({"display":"block"})

        $.post("/transaksicepat/handle_transaksi/", data = {
            "nama_kupon_dipilih" : $(".chosen-kupon").find("#specific-nama-kupon").text() ? $(".chosen-kupon").find("#specific-nama-kupon").text() : "None",
        }).done((res) => {
            $(".alert").css({
                "display":"none"
            })

            if (res["status"] == "success") {
                $(".alert-success").css({
                    "display":"block"
                })

                $("#checkout-btn").attr("disabled",true)

            } else if (res["status"] == "failed") {
                $(".alert-danger").css({
                    "display":"block"
                })

                $("#nama_barang_gagal").text(
                    res["failed_items"].join(",")
                )
            }
        })

    })
    $(".coupon-list").click((e) => {

        if ($(e.target).attr("id") == "specific-kupon") {
            $(".coupon-list").children().removeClass("chosen-kupon")
            $(e.target).addClass("chosen-kupon")

            $("#nama-kupon-transaksi").text($(e.target).find("#specific-nama-kupon").text())

            let kupon_percentage = Number($(e.target).find("#specific-percentage-kupon").text())

            $("#total-harga").css({
                "text-decoration" : "line-through"
            })

            $("#new-harga").text("Discounted price: ")
            $("#new-harga").append(`<span id="new-harga-amount">${String(kupon_percentage)}% off</span>`)

        }
    })

    $("#cari-kupon-btn").click(() => {
        var coupon_name = $("#cari-kupon-name").val()
        var coupon_percentage = Number($("#cari-kupon-percentage").val())
        var total_harga = Number($("#total-harga").text())
        
        $(".coupon-list").empty()
        $(".coupon-list").append(
            "<h4>Searching...</h4>"
        )

        $.post(
            "/coupon/",
            data = {
                name : coupon_name,
                percentage : coupon_percentage,
                total_harga : total_harga
            }
        ).done((res) => {
            $(".coupon-list").empty()
            for (let nama_kupon in res) {
                $(".coupon-list").append(
                    `<div id="specific-kupon" class="m-2 p-3 bg-warning rounded-lg">
                        <span id="specific-nama-kupon">${nama_kupon}</span> (<span id="specific-percentage-kupon">${res[nama_kupon]["percentage"]}</span>% off for a minimum purchase of Rp. ${res[nama_kupon]["minimum_price"]})
                    </div>`
                )
            }

            if (jQuery.isEmptyObject(res)) {
                $(".coupon-list").append(
                    "<p>No matching results.</p>"
                )
            }

            
        }
        )
    })

    
});
