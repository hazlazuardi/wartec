// Global Event for Rank System
// Cart Button Event
$(".showCart").on("click", function() {
    showCart();
    console.log('button seecart')
});
// Modal Button Event
// To close modal if user click outside the modal
$(".closeModal").on("click", function() {
    if ($('.modal').css("display", "flex")) {
        $('.modal').css("display", "none");
    }
    console.log('kepencet cart')
})

function showCart() {
    //  Make sure to empty up the modal first
    $("#cartResult").empty()
    $(".modal").css('display', 'flex');
    $.ajax({
        url: $("meta[name='cartListCartMeta']").attr("content"),
        type: "GET",
        success: function(res) {
            // console.log(res)
            console.log(res.data)
            var tempo = res.data
            var total_price = 0;
            for (i in tempo) {
                total_price += parseInt(tempo[i].price) * parseInt(tempo[i].quantity)
                var temp = '<div class="product1"><div class="product1">'


                console.log(parseInt(tempo[i].price) * parseInt(tempo[i].stock))
                temp += '<div class="cartResultDetail"><h4>' + tempo[i].product_name + '</h4><br><p>' + tempo[i].price +
                    "</p></div><div class='cartResultQty'><button id='" + tempo[i].entry_id + "'class='deleteItemQty'>-</button><h5>" + tempo[i].quantity +
                    "</h5><button id='" + tempo[i].entry_id + "' class='addItemQty'>+</button></div>"
                $("#cartResult").append(temp);
            }
            $(".modalContainer").append("</div>");
            $(".totalPrice").append("Total : " + total_price);

        }
    })

}





$("#cartResult").click((e) => {
    if ($(e.target).hasClass("deleteItemQty")) {
        $.ajax({
            url: $("meta[name='deleteItemQtyCartMeta']").attr("content"),
            type: "POST",
            data: {
                id_target: $(e.target).attr("id")
            },
            success: function() {
                showCart()
                alert("Product dikurangi")
            },
        })
    } else if ($(e.target).hasClass("addItemQty")) {
        $.ajax({
            url: $("meta[name='addItemQtyCartMeta']").attr("content"),
            type: "POST",
            data: {
                id_target: $(e.target).attr("id")
            },
            success: function() {
                showCart()
                alert("Item ditambahkan")
            }
        })
    }
})

// Button AddToCart
$("#addToCart").click(function() {
    var holder = $(".product_name").attr("id")
        // alert(holder)
    $.post($("meta[name='createCartMeta']").attr("content"), {
        product_name: holder,
    }).done(() => {
        alert("Product berhasil ditambahkan ke keranjang")
    })
})